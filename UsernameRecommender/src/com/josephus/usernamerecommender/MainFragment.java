package com.josephus.usernamerecommender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.josephus.usernamerecommender.ternarysearchtree.TernarySearchTree;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {

	private static final String TAG = MainFragment.class.getSimpleName(); 

	private TextView messageTxt, timerTxt;
	private EditText emailEditText, newUsernameEditText;
	private Button btnCheck, btnAddUsername;

	private TernarySearchTree tst;

	private static MainFragment instance = null;

	public static MainFragment getInstance() {
		if (instance == null) {
			instance = new MainFragment();
		}
		return instance;
	}

	private MainFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		messageTxt = (TextView) rootView.findViewById(R.id.txtMessage);
		timerTxt = (TextView) rootView.findViewById(R.id.txtTimer);
		emailEditText = (EditText) rootView.findViewById(R.id.editEmail);
		btnCheck = (Button) rootView.findViewById(R.id.btnCheck);
		btnAddUsername = (Button) rootView.findViewById(R.id.add);
		newUsernameEditText = (EditText) rootView.findViewById(R.id.editAddUsername);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		new AsyncTask<Void, Long, Void>() {

			private InputStream is;
			private BufferedReader fileReader;
			long time;

			protected void onPreExecute() {
				messageTxt.setText("Parsing text file. Please wait...");
				btnCheck.setEnabled(false);
				btnAddUsername.setEnabled(false);
			};

			@Override
			protected Void doInBackground(Void... params) {
				time = System.nanoTime();
				tst = new TernarySearchTree();
				try {
					is = getActivity().getAssets().open("usernames.txt");
					fileReader = new BufferedReader(new InputStreamReader(is));
					String line = null;
					while ((line = fileReader.readLine()) != null) {
						// store
						tst.addUsername(line);
					}
					publishProgress(System.nanoTime() - time);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					if (is != null) {
						try {
							is.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (fileReader != null) {
						try {
							fileReader.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				return null;
			}

			@Override
			protected void onProgressUpdate(Long... values) {
				timerTxt.setVisibility(View.VISIBLE);
				timerTxt.setText("process took " + values[0] + " ns");
			};

			protected void onPostExecute(Void result) {
				messageTxt.setText("Parsing done. Ready to recommend usernames.");
				btnCheck.setEnabled(true);
				btnAddUsername.setEnabled(true);
			};

		}.execute();

	}

	public void generateRecommendation() {
		long time = System.nanoTime();
		String username = emailEditText.getText().toString();
		int substringIndex = username.indexOf("@");
		if (substringIndex > 0) {
			username = username.substring(0, substringIndex);
		}
		username = username.replaceAll("\\d*$", "").toLowerCase().replace(" ", "");
		if (!tst.searchUsername(username)) {
			messageTxt.setText("username " + username + " is available.");
		} else {
			int index = 1;
			String newWord = username + index;
			while (tst.searchUsername(newWord)) {
				newWord = username + index++;
			}
			messageTxt.setText("username " + newWord + " is available.");
		}
		timerTxt.setText("process took " + (System.nanoTime() - time) + " ns");
		emailEditText.setText("");
	}

	public void addUsername() {
		long time = System.nanoTime();
		String username = newUsernameEditText.getText().toString().toLowerCase();
		username.replace(" ", "");
		tst.addUsername(username);
		messageTxt.setText("username " + username + " added.");
		timerTxt.setText("process took " + (System.nanoTime() - time) + " ns");
		newUsernameEditText.setText("");
	}
}