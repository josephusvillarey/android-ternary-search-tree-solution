package com.josephus.usernamerecommender.ternarysearchtree;
/** class TSTNode **/
class Node {
	char c;
	boolean isEndingNode;
	Node left, middle, right;

	public Node(char c) {
		this.c = c;
		this.isEndingNode = false;
		this.left = null;
		this.middle = null;
		this.right = null;
	}
}