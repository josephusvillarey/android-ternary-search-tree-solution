package com.josephus.usernamerecommender.ternarysearchtree;

import java.util.ArrayList;

public class TernarySearchTree {
	private Node root;
	private ArrayList<String> list;

	public TernarySearchTree() {
		root = null;
	}

	public boolean isEmpty() {
		return root == null;
	}

	public void clear() {
		root = null;
	}

	public void addUsername(String word) {
		root = insert(root, word.toCharArray(), 0);
	}

	public Node insert(Node node, char[] wordArray, int pointer) {
		if (node == null)
			node = new Node(wordArray[pointer]);

		if (wordArray[pointer] < node.c)
			node.left = insert(node.left, wordArray, pointer);
		else if (wordArray[pointer] > node.c) {
			node.right = insert(node.right, wordArray, pointer);
		} else {
			if (pointer + 1 < wordArray.length) {
				node.middle = insert(node.middle, wordArray, pointer + 1);
			} else {
				node.isEndingNode = true;
			}
		}
		return node;
	}

	public void removeUsername(String word) {
		removeUsername(root, word.toCharArray(), 0);
	}

	private void removeUsername(Node r, char[] word, int ptr) {
		if (r == null) {
			return;
		}
		if (word[ptr] < r.c) {
			removeUsername(r.left, word, ptr);
		} else if (word[ptr] > r.c) {
			removeUsername(r.right, word, ptr);
		} else {
			if (r.isEndingNode && ptr == word.length - 1) {
				r.isEndingNode = false;
			} else if (ptr + 1 < word.length) {
				removeUsername(r.middle, word, ptr + 1);
			}
		}
	}

	public boolean searchUsername(String word) {
		return searchUsername(root, word.toCharArray(), 0);
	}

	private boolean searchUsername(Node r, char[] word, int ptr) {
		if (r == null)
			return false;

		if (word[ptr] < r.c) {
			return searchUsername(r.left, word, ptr);
		} else if (word[ptr] > r.c) {
			return searchUsername(r.right, word, ptr);
		} else {
			if (r.isEndingNode && ptr == word.length - 1) {
				return true;
			} else if (ptr == word.length - 1) {
				return false;
			} else {
				return searchUsername(r.middle, word, ptr + 1);
			}
		}
	}

	public String toString() {
		list = new ArrayList<String>();
		traverse(root, "");
		return "\nTernary Search Tree : " + list;
	}

	private void traverse(Node r, String str) {
		if (r != null) {
			traverse(r.left, str);

			str = str + r.c;
			if (r.isEndingNode)
				list.add(str);

			traverse(r.middle, str);
			str = str.substring(0, str.length() - 1);

			traverse(r.right, str);
		}
	}
}