# README #

**Instructions**

Import and run as android application. 
Project needs the android-support-v7-appcompat library. You can get this from android-sdk\extras\android\support\v7\appcompat.

Signed APK is also located in apk folder and in the [Downloads section](https://bitbucket.org/josephusvillarey_/android-ternary-search-tree-solution/downloads). Just install and run on an android device.

---

The app pre-processes the usernames file (located in assets folder) by inserting them all in a TernarySearchTree data structure.

After the pre-processing (which will take a couple of seconds), the app is now ready to suggest usernames based on parsed email address. Suggesting a username will take between 0ms to 3ms.

Converts all input to lowercase before processing.

Insert function only inserts on the current data store and does not edit the usernames file. Closing and re-launching the app will cause another pre-process, reading the original usernames.txt file.

Native java implementation of Ternary Search Tree for the same problem can be found [here](https://bitbucket.org/josephusvillarey/ternarysearchtree).


Native java implementation of trie [here](https://bitbucket.org/josephusvillarey/trie-java-solution).